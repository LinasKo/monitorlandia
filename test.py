from slack_messenger import SlackMessenger
from random import randint
from time import sleep

# TODO: pull from secret
SLACK_TOKEN = ""
SLACK_CHANNEL = '#dl-monitor'

if __name__ == '__main__':
    messenger = SlackMessenger(SLACK_TOKEN, SLACK_CHANNEL)    
    percent_complete = 0
    bar_str = messenger.make_bar(percent_complete)
    messenger.post(bar_str) 
    while percent_complete < 100:
        bar_str = messenger.make_bar(percent_complete)
        messenger.update(bar_str)
        percent_complete += randint(10,30)
        sleep(1)
    percent_complete = 100
    bar_str = messenger.make_bar(percent_complete)
    messenger.update(bar_str)
    sleep(1)
    messenger.upload_file("./claptrap_536.jpeg", channels=[SLACK_CHANNEL])
